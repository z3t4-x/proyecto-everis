package com.api.accounts.services;

import java.util.List;

import com.api.accounts.model.Accounts;

public interface IAccountsService {

	
	List<Accounts> listar() throws Exception;
	Accounts listarPorId(String id) throws Exception;
}

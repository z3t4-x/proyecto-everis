package com.api.accounts.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.api.accounts.model.Accounts;

public interface IAccountsRepo extends MongoRepository<Accounts, String>{

}

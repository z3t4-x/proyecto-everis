package com.api.accounts.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.accounts.model.Accounts;
import com.api.accounts.services.IAccountsService;

import io.reactivex.Observable;

@RestController
@RequestMapping("/accounts")
public class AccountController {

	@Autowired
	private IAccountsService service;
	

	
	@GetMapping
	public ResponseEntity<List<Accounts>> listar() throws Exception {
		List<Accounts> lista  =  service.listar();
		
		
		
		Thread.sleep(5000);

        //return Single.just(personList);
		return new ResponseEntity<List<Accounts>>(lista ,  HttpStatus.OK);
	}
	
	

}

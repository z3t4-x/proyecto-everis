package com.api.cards.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.cards.model.Cards;
import com.api.services.ICardService;

import io.reactivex.Single;
import lombok.SneakyThrows;
@RestController
@RequestMapping("/cards")
public class CardController {

	
	
	@Autowired
	private ICardService service;
	
	@GetMapping
	public ResponseEntity<List<Cards>> listar() throws Exception {
		List<Cards> lista  =  service.listar();
		return new ResponseEntity<List<Cards>>(lista, HttpStatus.OK);
	}
	
	
	
	/*
    @SneakyThrows
    @GetMapping(
            value = "/prueba/",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<List<Cards>> getAccounts(@RequestHeader("Authorization") String header) {

    	List<Cards> lista  =  service.listar();

        Thread.sleep(3000);

        return Single.just(lista);
    }
	*/
	
	
}

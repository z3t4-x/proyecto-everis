package com.api.fingerprints.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class Fingerprints {

	@Id
	private String _id;
	@Indexed(unique = true)
	private String document;
	private String entityname;
	private boolean success;
}

package com.api.fingerprints.servicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.fingerprints.model.Fingerprints;
import com.api.fingerprints.repo.IFingerprintRepo;
import com.api.fingerprints.services.IFingerprintService;

@Service
public class IFingerprintServiceImpl implements IFingerprintService {

	@Autowired
	private IFingerprintRepo repo;
	
	@Override
	public Fingerprints registrar(Fingerprints f) {
		
		
		return repo.save(f);
	}

	@Override
	public Fingerprints modificar(Fingerprints f) {
		return repo.save(f);
	}

	@Override
	public List<Fingerprints> listar() {
		return repo.findAll();
	}



	@Override
	public Fingerprints listarPorId(String id) {
		Optional<Fingerprints> op =  repo.findById(id);
		return op.isPresent() ? op.get() : new Fingerprints();
	}

	@Override
	public Fingerprints listarPorDoc(String doc)  {
		Optional<Fingerprints> op =  repo.findByDocument(doc);
		return op.isPresent() ? op.get() : new Fingerprints();
	}

}

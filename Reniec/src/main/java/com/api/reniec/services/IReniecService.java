package com.api.reniec.services;

import java.util.List;

import javax.validation.Valid;

import com.api.reniec.model.Reniec;

public interface IReniecService  {

	
	Reniec registrar(Reniec r) throws Exception;
	Reniec modificar(Reniec r) throws Exception;
	List<Reniec> listar() throws Exception;
	Reniec listarPorId(String id) throws Exception;
	Reniec listarPorDocxx(String doc) throws Exception;
	List<Reniec> listarDoc(String doc) throws Exception;
	
}

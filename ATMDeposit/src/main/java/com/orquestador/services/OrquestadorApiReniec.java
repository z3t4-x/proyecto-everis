package com.orquestador.services;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

import com.orquestador.model.ATMReniec;
import com.orquestador.model.Persons;

import io.reactivex.Single;
import lombok.SneakyThrows;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface OrquestadorApiReniec {

	
   

	@GET("/{document}")
    Single<List<ATMReniec>> validarApi(@Path("document") String user);
	
	
	
    }
    





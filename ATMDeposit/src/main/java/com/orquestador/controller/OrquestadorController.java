package com.orquestador.controller;

import java.util.List;
import java.util.concurrent.Executors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.orquestador.model.Persons;
import com.orquestador.services.OrquestadorApi;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Slf4j
@RestController
@RequestMapping("/core")
@RequiredArgsConstructor
public class OrquestadorController {

	 

	private static final Logger log = LoggerFactory.getLogger(OrquestadorController.class);
	
	
	private final OrquestadorApi orquestadorApi;
	
	@GetMapping("/persons/{persons}")
    public Single<List<Persons>> getStatisticsForUser(@PathVariable("persons")  String user) {
   
		log.info(user);
		
		
        Scheduler scheduler = Schedulers.from(Executors.newFixedThreadPool(2));

        
        List<Persons> lista = (List<Persons>) orquestadorApi.validarApi(user).subscribeOn(scheduler) ;
        
       return  Single.just(lista);
    }
	
	
	
}

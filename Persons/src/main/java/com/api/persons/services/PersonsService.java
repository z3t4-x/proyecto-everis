package com.api.persons.services;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;

import com.api.persons.model.Persons;

public interface PersonsService {

	

	Persons registrar(Persons p);
	Persons modificar(Persons p);
	List<Persons> listar();
	Persons listarPorId(String id);
	Persons findByDoc(String doc);
	
	 
	
	/*
	 @Query ("{'document':? 0}")
	 List <Persons> listarPorDocument (String doc);
	*/
	
	

	
}

package com.api.persons.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class Persons {

	@Id
	 private String id;
	
	 @Indexed(unique = true)
	 private String document;
	 private boolean fingerprint;
	 private boolean blacklist;

}

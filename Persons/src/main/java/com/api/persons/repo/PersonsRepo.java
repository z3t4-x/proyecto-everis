package com.api.persons.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.api.persons.model.Persons;

public interface PersonsRepo extends MongoRepository<Persons, String> {

	@Query("{ 'document' : { $regex: ?0, $options:'i' } }")
	// select campo1, campo2 from tabla @Query(value="{ 'document' : ?0 }", fields="{ 'fingerprint' : 1, 'blacklist' : 1}")
	Optional<Persons> findByDocument(String doc);
	//  List<Persons> searchDocument(String doc);
/*
    @Query("select c from persons c where c.document = :document")
    Stream<Persons> findByDocumentReturnStream(@Param("documento") String documento);
	*/
}
